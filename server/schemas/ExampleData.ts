export interface ExampleData {
  index: number
  text: string
  status: boolean
}

export default toExampleSchema<ExampleData>({
  index: 0,
  text: 'text',
  status: false,
}, {
  index: 'Description for index',
  text: 'Description for text',
  status: 'Description for status',
}, {
  description: 'Example data',
})
