import { createOpenApiRegister } from '@byyuurin/nitro-openapi'
import type { OpenAPI3 } from 'openapi-typescript'
import schemas from '../schemas'

export const { register, merge, defineOperation, configExtends } = createOpenApiRegister({
  info: {
    title: 'Swagger API',
    version: 'dev',
  },
  servers: [
    { url: 'https://example-nitro-openapi.netlify.app', description: 'example api', variables: {} },
  ],
  tags: [
    { name: 'API Routes', description: 'All provided APIs' },
    { name: 'App Routes' },
    { name: 'Internal' },
  ] as const,
  components: {
    securitySchemes: {
      FrontendToken: {
        type: 'http',
        description: 'Frontend JWT Token',
        scheme: 'bearer',
      },
      BackendToken: {
        type: 'http',
        description: 'Backend JWT Token',
        scheme: 'bearer',
      },
    },
    schemas,
  },
})

export default defineNitroPlugin((nitro) => {
  nitro.hooks.hook('error', (error) => {
    // eslint-disable-next-line no-console
    console.log('nitro error:', error.message)
  })

  nitro.hooks.hook('beforeResponse', (event, context) => {
    // config adjust
    if (event.path.endsWith('swagger')) {
      let body = context.body as string

      const settings = Object.entries({
        // docExpansion: 'none', // collapse all groups
        // defaultModelsExpandDepth: -1, // hide schemas
      }).map(([k, v]) => `$1${k}: ${typeof v === 'string' ? `"${v}"` : v}`).join('')

      body = body.replace(/(\s*)(url:\s"[^"]+")/, `${settings ? `${settings},` : ''}$1$2`)
      // console.log(body.match(/(?:SwaggerUIBundle\(([^;]+)\))/)?.[1])

      // NOTE: override swagger-ui version until upgraded to nitorpack v2.10
      body = body.replaceAll('dist@^4', 'dist@^5')

      context.body = body
      return
    }

    if (!/openapi.json/.test(event.path))
      return

    // merge config
    const config = context.body as OpenAPI3
    context.body = merge(config)
  })
})
