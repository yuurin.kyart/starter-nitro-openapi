// https://nitro.unjs.io/config
export default defineNitroConfig({
  srcDir: 'server',
  experimental: {
    openAPI: true,
  },
  routeRules: {
    '/api/**': { cors: true },
  },
})
